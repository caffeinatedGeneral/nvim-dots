" Some basics:
	set nocompatible " Removes vi compatibility mode
	colorscheme peachpuff
	filetype plugin indent on " Enables plugins
	syntax enable
	set encoding=utf-8
	set number
	set numberwidth=2
	set shiftround
	set statusline+=%F  " Adds full filepath to status line
	set path+=**  " Enables Fuzzy :find searching in current directory/sub directory
	highlight Comment cterm=italic

" Splits open at the bottom and right.
	set splitbelow
	set splitright

" Open & source vimrc
	nnoremap <leader>ev :vsplit $MYVIMRC<cr>
    nnoremap <leader>sv :source $MYVIMRC<cr>

" Replace all is aliased to S.
	nnoremap S :%s//g<Left><Left>

" Readmes autowrap text:
	autocmd BufRead,BufNewFile *.md,*.tex set tw=79

" Get line, word and character counts with F3:
	map <F3> :!wc <C-R>%<CR>

" Spell-check set to F6:
	map <F6> :setlocal spell! spelllang=en_us<CR>

" Enable autocompletion for Vim Commands:
	set wildmode=longest,list,full
	set wildmenu

" Automatically deletes all tralling whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" C-T for new tab
	nnoremap <C-t> :tabnew<cr>

" Bubble lines in Visual mode
	vnoremap K xkP`[V`]
	vnoremap J xp`[V`]

" Indent lines in Visual mode
	vnoremap L >gv
	vnoremap H <gv

" Touppercase in normal/insert mode
	inoremap <C-u> <esc>vbUA
	nnoremap <C-u> viwU<esc>

" WIP Comment out lines
	vnoremap - <C-n>I//<Esc>
	vnoremap _ <C-n>2x

" Enable folding
	set foldmethod=indent
	set foldnestmax=10
	set nofoldenable
	set foldlevel=2

filetype plugin indent on

" Set indenting to 4 spaces for tab and >
	set tabstop=4
	set shiftwidth=4
	set expandtab

" Set Cargo to compile with :make
	set makeprg=cargo\ run

" Netrw settings"
	let g:netrw_banner = 0
	let g:netrw_liststyle = 3
	let g:netrw_browse_split = 4
	let g:netrw_altv = 1

"Addons"

call plug#begin('~/.config/nvim/plugged')

" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'

" Completion framework
Plug 'hrsh7th/nvim-cmp'

" LSP completion source for nvim-cmp
Plug 'hrsh7th/cmp-nvim-lsp'

" Snippet completion source for nvim-cmp
Plug 'hrsh7th/cmp-vsnip'

" Other usefull completion sources
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'

" See hrsh7th's other plugins for more completion sources!

" To enable more of the features of rust-analyzer, such as inlay hints and more!
Plug 'simrat39/rust-tools.nvim'

" Snippet engine
Plug 'hrsh7th/vim-vsnip'

call plug#end()

" Set completeopt to have a better completion experience
" :help completeopt
" menuone: popup even when there's only one match
" noinsert: Do not insert text until a selection is made
" noselect: Do not select, force user to select one from the menu
set completeopt=menuone,noinsert,noselect

" Avoid showing extra messages when using completion
set shortmess+=c

" Configure LSP through rust-tools.nvim plugin.
" rust-tools will configure and enable certain LSP features for us.
" See https://github.com/simrat39/rust-tools.nvim#configuration
lua <<EOF
local nvim_lsp = require'lspconfig'

local opts = {
    tools = { -- rust-tools options
        autoSetHints = true,
        hover_with_actions = true,
        inlay_hints = {
            show_parameter_hints = false,
            parameter_hints_prefix = "",
            other_hints_prefix = "",
        },
    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
    server = {
        -- on_attach is a callback called when the language server attachs to the buffer
        -- on_attach = on_attach,
        settings = {
            -- to enable rust-analyzer settings visit:
            -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
            ["rust-analyzer"] = {
                -- enable clippy on save
                checkOnSave = {
                    command = "clippy"
                },
            }
        }
    },
}

require('rust-tools').setup(opts)
EOF

" Setup Completion
" See https://github.com/hrsh7th/nvim-cmp#basic-configuration
lua <<EOF
cmp.setup({
local cmp = require'cmp'
  -- Enable LSP snippets
  snippet = {
    expand = function(args)
        vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    -- Add tab support
    ['<S-Tab>'] = cmp.mapping.select_prev_item(),
    ['<Tab>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    })
  },

  -- Installed sources
  sources = {
    { name = 'nvim_lsp' },
    { name = 'vsnip' },
    { name = 'path' },
    { name = 'buffer' },
  },
})
EOF

" Show information/help
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
" Go to implementation
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
" Show more information/help
nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
" Show type definition?
nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
" Show all references
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
" Show all symbols in document
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
" Show specific symbol document
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
" Go to the definition
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.definition()<CR>
" Prompt for code actions
nnoremap <silent> ga    <cmd>lua vim.lsp.buf.code_action()<CR>
" Go to next e& Previous error
nnoremap <silent> g[ <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> g] <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

" have a fixed column for the diagnostics to appear in
" this removes the jitter when warnings/errors flow in
set signcolumn=yes

"#####https://sharksforarms.dev/posts/neovim-rust/
"Testing"

" Signature Abbreviations
	iabbrev ssig <cr>Caffeinated General<cr>caffeinatedgeneral@tutanoa.com

" Disable arrow keys
	noremap <up>	<nop>
	noremap <down>  <nop>
	noremap <left>  <nop>
	noremap <right> <nop>

	inoremap <up>	<nop>
	inoremap <down>  <nop>
	inoremap <left>  <nop>
	inoremap <right> <nop>


