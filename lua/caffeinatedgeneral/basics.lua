local opt = vim.opt

vim.cmd('colorscheme peachpuff')

opt.number = true
opt.relativenumber = true

opt.tabstop = 4
opt.shiftwidth = 4
opt.expandtab = true

opt.hidden = true

opt.signcolumn = 'number'
opt.hlsearch = false

opt.cursorline = true
opt.cursorlineopt = 'line'
vim.cmd('hi CursorLine cterm=NONE ctermbg=235')

-- undo, backup and swp info https://begriffs.com/posts/2019-07-19-history-use-vim.html#backups-and-undo
-- Using true path since Expansion doesn't seem to work with lua configs
opt.undofile = true
opt.undodir = "/home/john/.config/nvim/.undo//"
opt.writebackup = true
opt.backup = false
opt.backupcopy = 'auto'
opt.backupdir = "/home/john/.config/nvim/.backup//"
opt.directory = "/home/john/.config/nvim/.swp//"

opt.spell = true
vim.cmd('hi clear SpellBad')
vim.cmd('hi SpellBad cterm=underline')

opt.title = true

opt.ignorecase = true
opt.smartcase = true

opt.wildmenu = true
opt.wildmode = 'list:longest,full'

opt.wrap = false

opt.scrolloff = 8
opt.sidescrolloff = 8

opt.list = true
opt.listchars = {tab = '▸\\', trail = '·', extends = '→', precedes = '←'}
vim.cmd('hi NonText ctermfg=DarkGrey')

vim.cmd('hi Pmenu ctermbg=236')
vim.cmd('hi Pmenu ctermfg=White')

opt.splitright = true
opt.splitbelow = true

opt.mouse = 'a'

opt.updatetime = 750

opt.clipboard = 'unnamedplus'

opt.completeopt={"menu", "menuone", "noselect", "noinsert"}
