vim.g.mapleader = " "

local opts = { noremap = true}
-- reselect line when changing indentation and adding it to H and L
vim.keymap.set("v", '<', '<gv', opts)
vim.keymap.set('v', '>', '>gv', opts)
vim.keymap.set('v', 'H', '<gv', opts)
vim.keymap.set('v', 'L', '<gv', opts)

-- fix cursor position moving on yank
vim.keymap.set('v', 'y', 'myy`y', opts)
vim.keymap.set('v', 'Y', 'myY`y', opts)

-- center searched line
vim.keymap.set('n', 'n', 'nzzzv', opts)
vim.keymap.set('n', 'N', 'Nzzzv', opts)
vim.keymap.set('n', 'J', 'mzJ`z', opts)

-- Bubble or pop v selected line
vim.keymap.set('v', 'K', 'xkP`[V`]', opts)
vim.keymap.set('v', 'J', 'xp`[V`]', opts)

-- add ; or , at the end of the line while in insert mode
vim.keymap.set('i', ';;', '<Esc>A;<Esc>', opts)
vim.keymap.set('i', ',,', '<Esc>A,<Esc>', opts)

-- open hovered file with xdg or edit
vim.keymap.set('n', '<leader>x', ':!xdg-open %<CR><CR>', opts)
vim.keymap.set('n', 'gf', ':edit <cfile><CR>', { noremap = false })

-- Open terminal and enable Esc
vim.keymap.set('n', '<leader><CR>', ':sp<CR>:term<CR>:resize 20N<CR> i', opts)
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>', opts)

-- init edit and reload
vim.keymap.set('n', '<leader>ve', ':Telescope find_files cwd=$HOME/.config/nvim/<CR>', opts)
vim.keymap.set('n', '<leader>vr',
    ':lua package.loaded.caffeinatedgeneral = nil<CR>:source $HOME/.config/nvim/init.vim<CR>', opts)

-- add X in [] for todo list notes
vim.keymap.set('n', '<leader>dt', 'T[iX<Esc>', opts)

-- Regex
vim.keymap.set('n', '<leader>S', ':%s///g<left><left><left>', opts)

-- Change working directory to files working director
vim.keymap.set('n', '<leader>cd', ':lcd %:p:h<CR>:pwd<CR>')

-- Disable grammar check
vim.keymap.set('n', '<F6>', ':setlocal spell! spelllang=en_us<CR>', { noremap = false })

-- Telescope
vim.keymap.set('n', '<leader>ff', ':Telescope find_files<CR><Esc>', opts)
vim.keymap.set('n', '<leader>/', ':Telescope current_buffer_fuzzy_find<CR><Esc>', opts)
vim.keymap.set('n', '<leader>gf', ':Telescope git_files<CR><Esc>', opts)
vim.keymap.set('n', '<leader>gs', ':Telescope git_status<CR><Esc>', opts)
vim.keymap.set('n', '<leader>gc', ':Telescope git_commits<CR><Esc>', opts)
vim.keymap.set('n', '<leader>gb', ':Telescope git_branches<CR><Esc>', opts)


vim.keymap.set('n', '<leader>fb', ':Telescope buffers<CR><Esc>', opts)
vim.keymap.set('n', '<leader>kb', ':Telescope keymaps<CR><Esc>', opts)
